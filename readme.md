# Fractional Calculator
This is a .Net core console application that can perform addition, subtraction, division and multiplication on 2 fractional numbers. 

## Coding problem
Write a command line program in the language of your choice (preferably Java or C#) that will take operations on fractions as an input and produce a fractional result.

- Legal operators shall be *, /, +, - (multiply, divide, add, subtract)
- Operands and operators shall be separated by one or more spaces
- Mixed numbers will be represented by whole_numerator/denominator. e.g. "3_1/4"
- Improper fractions and whole numbers are also allowed as operands 


Example run:

? 1/2 * 3_3/4

= 1_7/8

? 2_3/8 + 9/8

= 3_1/2

## Assumptions
- Denominator can never be zero
- If the fraction is a mixed number, only the whole number portion can be negative
- For fractions without whole numbers, negatives are allowed on numerators as well as denominators
- Any characters other than the following are invalid. Entering any invalid characters will return an error:
    - Valid characters: 1-9, *, /, +, -, _
- Incomplete fractions will be considered as invalid as well and will return an error
    - Example of incomplete/invalid fractions in a run: 
        - 1/ * 2
        - /3 * 1/5
        - 1/3/2 * 8
- Only up to 2 operands are allowed
- If there is only one operand, the app will just simplify the fraction
- If there is no space between an operand and operators, the run will return an error
    - Example:
        - 1/2*3/4

## Running the published app
The published app can be found under bin/release/netcoreapp2.1/win10-x64

The app can be run by running: fractional-calculator.exe

Please note that for the exe to work, all files in the win10-x64 folder will have to be downloaded.

## Running the app from the source code
Since .Net Core is cross-platform, it can be run on Windows, macOS and multiple distributions of Linux.

To run the app an installation of .Net Core SDK is required.
- [Download link](https://www.microsoft.com/net/download)

After installing .Net Core SDK, the app can be run from the base directory of the project by using the following dotnet cli command: 
```sh
$ dotnet run
``` 

## Debugging the app
Supporting .vscode files to build the app in VS Code have also been provided. To debug the app in VS Code, open the root folder in the code editor and run the code by hitting F5.