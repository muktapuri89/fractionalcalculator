﻿using System;
using System.Text;

namespace fractional_calculator
{
    public class Fraction
    {
        public int Numerator;
        public int Denominator;
        public int WholeNumber;

        public Fraction() { }
        
        public Fraction(string stringNumber)
        {
            string stringFraction;
            var stringNumberParts = stringNumber.Split('_');

            if (stringNumberParts.Length > 2)
            {
                throw new Exception("Cannot parse number.");
            }
            if (stringNumberParts.Length == 2)
            {
                try 
                {
                    this.WholeNumber = Int32.Parse(stringNumberParts[0]);
                    stringFraction = stringNumberParts[1];
                }
                catch (Exception e)
                {
                    throw new Exception($"Cannot parse Numbers. Exception: {e.Message}");
                }
            }
            else
            {
                stringFraction = stringNumberParts[0];
            }
            
            var fractionParts = stringFraction.Split('/');

            if (fractionParts.Length > 2)
            {
                throw new Exception("Cannot parse Numbers. Invalid fraction.");
            }
            else
            {
                try 
                {
                    this.Numerator = Int32.Parse(fractionParts[0]);   
                    if (fractionParts.Length == 1)
                    {             
                        this.Denominator = 1;
                    }
                    else 
                    {
                        this.Denominator = Int32.Parse(fractionParts[1]);

                        if (this.Denominator == 0)
                        {
                            throw new Exception($"Invalid fraction. Denominator is Zero.");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Cannot parse Numbers. Exception: {e.Message}");
                }
            } 
        }
        
        public override string ToString()
        {
            StringBuilder resultStringBuilder = new StringBuilder();

            if (this.WholeNumber == 0 && this.Numerator == 0 && this.Denominator == 0)
            {
                resultStringBuilder.Append(0);
            } 
            else{  
                if (this.WholeNumber != 0)
                {
                    resultStringBuilder.Append(this.WholeNumber);

                    if (this.Numerator != 0)
                    {
                        resultStringBuilder.Append("_");
                    }
                }
                
                if (this.Numerator != 0 && this.Denominator != 0)
                {
                    resultStringBuilder.Append(this.Numerator);
                    resultStringBuilder.Append("/");
                    resultStringBuilder.Append(this.Denominator);
                }
            } 

            return resultStringBuilder.ToString();
        }

        public Fraction Cleanup(){
            return this.ToImproperFraction().ToMixedNumber();
        }
        
        public Fraction ToMixedNumber()
        {
            if (this.Denominator == 0)
            {
                throw new Exception("Invalid fraction. Denominator is zero. Cannot convert to mixed number.");
            }

            SimplifyFraction();

            // Use Abs for negative numbers
            if (System.Math.Abs(this.Numerator) < System.Math.Abs(this.Denominator))
            {
                return this;
            }
            else if (this.Numerator != 0)
            {
                var mixedNumber = new Fraction();

                // To handle cases where the input fraction might also have a whole number.
                // This could technically be called an invalid case for this function but I'm still supporting it in case an input like this is entered: 5_3/2
                if (this.WholeNumber != 0) {
                    mixedNumber.WholeNumber = this.WholeNumber;
                }

                if (this.Denominator == 1)
                {
                    mixedNumber.WholeNumber += this.Numerator;
                }
                else 
                {
                    mixedNumber.WholeNumber += this.Numerator / this.Denominator;

                    // Abs used in case the improper fraction is a negative number. 
                    // The whole number portion already contains the negative number.
                    mixedNumber.Numerator = System.Math.Abs(this.Numerator % this.Denominator);
                    mixedNumber.Denominator = this.Denominator;
                }

                return mixedNumber;
            }
            else 
            {
                return this;
            }
        }
        
        public Fraction ToImproperFraction()
        {
            if (this.WholeNumber == 0)
            {
                return this;
            }
            else if (this.Numerator < 0 || this.Denominator < 0)
            {
                throw new Exception("Invalid negatives in mixed number.");
            }
            else 
            {
                if (this.Denominator == 0) 
                {
                    throw new Exception("Invalid fraction. Denominator is zero. Cannot convert to improper fraction.");
                }

                var improperFraction = new Fraction();

                if (this.Numerator == 0){
                    improperFraction.Numerator = this.WholeNumber;
                    improperFraction.Denominator = 1;
                }
                else 
                {
                    improperFraction.Numerator = 
                        System.Math.Abs(this.WholeNumber * this.Denominator) + this.Numerator;
                    
                    if (this.WholeNumber < 0){
                        improperFraction.Numerator = improperFraction.Numerator * -1;
                    }
                    improperFraction.Denominator = this.Denominator;
                }
                return improperFraction;
            }
        }

        public static Fraction Multiply(Fraction operand1, Fraction operand2)
        {
            if (operand1.WholeNumber != 0)
            {
                operand1 = operand1.ToImproperFraction();
            }
            
            if (operand2.WholeNumber != 0)
            {
                operand2 = operand2.ToImproperFraction();
            }
            
            var result = new Fraction();
            result.Numerator = operand1.Numerator * operand2.Numerator;
            result.Denominator = operand1.Denominator * operand2.Denominator;

            return result.ToMixedNumber();
        }
        
        public static Fraction Divide(Fraction operand1, Fraction operand2)
        {
            var reversedOperand2 = new Fraction();            
            
            if (operand2.WholeNumber != 0)
            {
                operand2 = operand2.ToImproperFraction();
            }

            reversedOperand2.Numerator = operand2.Denominator;
            reversedOperand2.Denominator = operand2.Numerator;

            return Multiply(operand1, reversedOperand2);
        }

        public static Fraction Add(Fraction operand1, Fraction operand2)
        {
            int leastCommonMultiplier = LeastCommonMultiplier(operand1.Denominator, operand2.Denominator);

            int operand1AddValue = GetAddOrSubtractNumeratorValue(operand1, leastCommonMultiplier);
            int operand2AddValue = GetAddOrSubtractNumeratorValue(operand2, leastCommonMultiplier);

            var additionResult = new Fraction();
            additionResult.Numerator = operand1AddValue + operand2AddValue;
            additionResult.Denominator = leastCommonMultiplier;

            return additionResult.ToMixedNumber();            
        }

        public static Fraction Subtract(Fraction operand1, Fraction operand2)
        {
            int leastCommonMultiplier = LeastCommonMultiplier(operand1.Denominator, operand2.Denominator);

            int operand1SubtractValue = GetAddOrSubtractNumeratorValue(operand1, leastCommonMultiplier);
            int operand2SubtractValue = GetAddOrSubtractNumeratorValue(operand2, leastCommonMultiplier);

            var subtractionResult = new Fraction();
            subtractionResult.Numerator = operand1SubtractValue - operand2SubtractValue;
            subtractionResult.Denominator = leastCommonMultiplier;

            return subtractionResult.ToMixedNumber();            
        }

        private void SimplifyFraction()
        {
            Fraction result = new Fraction();
            
            if (this.Denominator == 0) 
            {
                throw new Exception("Invalid fraction. Denominator is zero. Cannot simplify fraction.");
            }
            
            if (this.Numerator == 0) 
            {
                result.Numerator = 0;
                result.Denominator = 0;
            }
            else
            {
                int highestCommonFactor = HighestCommonFactor(this.Numerator, this.Denominator);
                
                if (highestCommonFactor != 0)
                {
                    result.Numerator = System.Math.Abs(this.Numerator/highestCommonFactor);
                    result.Denominator = System.Math.Abs(this.Denominator/highestCommonFactor);
                }

                // Simplify negatives.
                // If either the numerator or denominator are negative, the fraction is negative.
                if (this.Numerator < 0 ^ this.Denominator < 0)
                {
                    result.Numerator = result.Numerator * -1;
                }
            }
            
            this.WholeNumber = result.WholeNumber;
            this.Numerator = result.Numerator;
            this.Denominator = result.Denominator;
        }

        private static int GetAddOrSubtractNumeratorValue(Fraction operand, int leastCommonMultiplier)
        {
            if (operand.WholeNumber != 0)
            {
                operand = operand.ToImproperFraction();
            }
            
            return operand.Numerator * (leastCommonMultiplier/operand.Denominator);
        }

        private static int HighestCommonFactor(int number1, int number2)
        {
            number1 = System.Math.Abs(number1);
            number2 = System.Math.Abs(number2);
            
            if (number1 == 0 || number2 == 0)
            {
                return 0;
            }

            while(number1 != number2)
            {
                if(number1 > number2)
                {
                    number1 -= number2;
                }
                else
                {
                    number2 -= number1;
                }
            }

            return number1;
        }

        private static int LeastCommonMultiplier(int number1, int number2)
        {
            number1 = System.Math.Abs(number1);
            number2 = System.Math.Abs(number2);

            int highestCommonFactor = HighestCommonFactor(number1, number2);

            return (number1 * number2)/highestCommonFactor;
        }
    }
}
