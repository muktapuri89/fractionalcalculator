﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace fractional_calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputValid = false;
            var anotherCalculation = false;

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("         Fractional Calculator         ");
            Console.WriteLine("---------------------------------------");

            while (!inputValid || anotherCalculation) 
            {
                anotherCalculation = false;
                Console.WriteLine("\nEnter an operation on fractions: ");
                var input = Console.ReadLine();
                inputValid = ValidateInputCharacters(input);

                if (!inputValid)
                {
                    Console.WriteLine("Operation is not valid.");
                }
                else
                {
                    try {
                        var result = ParseInputStringAndCalculate(input);                         

                        Console.WriteLine("Calculation Result:");
                        Console.WriteLine(result);

                        Console.Write("Do you want to do another calculation? (y?): ");
                        var response = Console.ReadLine();
                        if (response == "y"|| response == "Y")
                        {
                            anotherCalculation = true;
                        }
                    } 
                    catch (Exception e)
                    {
                        Console.WriteLine($"Cannot perform calculation. {e.Message}");
                        inputValid = false;
                    }
                }
            }

            Console.WriteLine("Press any key to exit... ");
            Console.ReadLine();
        }

        private static string ParseInputStringAndCalculate(string input)
        {
            input = input.Trim();

            var inputParts = System.Text.RegularExpressions.Regex.Split(input, @"\s+");

            // For cases where there is only one operand, clean up the number
            if (inputParts.Length == 1){
                var result = new Fraction(inputParts[0]);
                return result.Cleanup().ToString();
            }
            // For cases where there are 2 operands, perform the calculation
            else if (inputParts.Length == 3)
            {
                var operand1 = new Fraction(inputParts[0]);
                var operand2 = new Fraction(inputParts[2]);

                var fractionOperation = inputParts[1];

                var result =  PerformCalculation(operand1, operand2, fractionOperation);

                return result.ToString();
            } 
            else
            {
                throw new Exception("Invalid operation. Please enter 2 operands separated by an operator. \nOperands and operators should be separated by one or more spaces.");
            }
        }

        private static Fraction PerformCalculation(Fraction operand1, Fraction operand2, string fractionOperation)
        {
            switch (fractionOperation) {
                case "*":
                    return Fraction.Multiply(operand1, operand2);
                case "/":
                    return Fraction.Divide(operand1, operand2);
                case "+":
                    return Fraction.Add(operand1, operand2);
                case "-":
                    return Fraction.Subtract(operand1, operand2);
                default:
                    throw new Exception("Invalid Operation.");
            }
        }

        private static bool ValidateInputCharacters(string input)
        {
            Regex pattern = new Regex("^[0-9*/+_ -]*$");
            return pattern.IsMatch(input);
        }

    }
}
